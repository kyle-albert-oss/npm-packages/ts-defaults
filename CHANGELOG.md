## [1.0.1](https://gitlab.com/kyle-albert-oss/npm-packages/ts-defaults/compare/v1.0.0...v1.0.1) (2020-02-24)

# 1.0.0 (2020-02-24)

### Features

- Initialize with new name. ([db6a7dc](https://gitlab.com/kyle-albert-oss/npm-packages/ts-defaults/commit/db6a7dc319778349b44c6458b8ec14e9f7a36af2))
