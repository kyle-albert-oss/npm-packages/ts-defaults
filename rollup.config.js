/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */
import autoExternal from "rollup-plugin-auto-external";
import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import multiInput from "rollup-plugin-multi-input";

import pkg from "./package.json";

const { SINGLE_FILE_BUNDLE, PACKAGE_TYPE, PACKAGE_TYPE_BROWSER, FILE_EXTENSIONS } = require("./build-config");

const entryPoint = SINGLE_FILE_BUNDLE ? "src/index.ts" : ["src/**/*.ts", "!src/**/*.test.ts", "!src/**/__tests__/**"];

const nodeResolveOpts = {
	// builtins: false, // uncomment for browser
	extensions: FILE_EXTENSIONS,
};
if (PACKAGE_TYPE === PACKAGE_TYPE_BROWSER) {
	nodeResolveOpts.builtins = false;
}

// order matters
const sharedPlugins = [
	SINGLE_FILE_BUNDLE ? null : multiInput(),
	autoExternal(),
	nodeResolve(nodeResolveOpts),
	commonjs(),
	typescript({
		tsconfig: "./tsconfig.dist.json",
	}),
].filter((p) => p != null);

const bundles = [
	// CommonJS (for Node) and ES module (for bundlers) build.
	// (We could have three entries in the configuration array
	// instead of two, but it's quicker to generate multiple
	// builds from a single configuration where possible, using
	// an array for the `output` option, where we can specify
	// `file` and `format` for each target)
	SINGLE_FILE_BUNDLE
		? {
				input: entryPoint,
				output: [
					{ file: pkg.main, format: "cjs" },
					{ file: pkg.module, format: "es" },
				],
				plugins: [...sharedPlugins],
		  }
		: {
				input: entryPoint,
				output: [
					{ dir: "dist/cjs", format: "cjs" },
					{ dir: "dist/esm", format: "es" },
				],
				plugins: [...sharedPlugins],
		  },
];

if (PACKAGE_TYPE === PACKAGE_TYPE_BROWSER && pkg.browser) {
	bundles.push({
		input: entryPoint,
		output: {
			name: "index",
			file: pkg.browser,
			format: "umd",
		},
		plugins: [...sharedPlugins],
	});
}

export default bundles;
