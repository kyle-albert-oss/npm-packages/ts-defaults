// StrictOmit credit: ts-essentials
type StrictOmit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
type RequiredAndDefined<T, K extends keyof T> = {
	[P in K]-?: Exclude<T[P], undefined>;
};
type MappedObjValue<A, B> = {
	[K in keyof A & keyof B]: A[K] extends B[K] ? never : K;
};
export type OptionalKeys<T> = MappedObjValue<T, Required<T>>[keyof T];
type KeyOrKeysOf<P> = ReadonlyArray<keyof P> | keyof P;
type ExtractArrayItem<T> = T extends ReadonlyArray<infer U> ? U : T;
export type OptionalsOnly<T> = Pick<T, OptionalKeys<T>>;

export type WithEnforcedDefaults<P extends object, K extends KeyOrKeysOf<P> | undefined, EK = ExtractArrayItem<K>> = [EK] extends [keyof P] ? StrictOmit<P, EK> & RequiredAndDefined<P, EK> : P;

export type ObjectDefaults<T extends object, K extends KeyOrKeysOf<OT> | undefined, OT extends OptionalsOnly<T> = OptionalsOnly<T>, EK = ExtractArrayItem<K>> = [EK] extends [keyof OT]
	? WithEnforcedDefaults<OT, EK>
	: OT;

export type EnforcedDefaultKeys<T> = ReadonlyArray<OptionalKeys<T>> | undefined;

function tsDefaults<T extends object, ED extends EnforcedDefaultKeys<T>>(obj: T, defaultObj: ObjectDefaults<T, ED>, enforcedDefaults?: ED): WithEnforcedDefaults<T, ED> {
	const newObj: T = Object.assign({}, defaultObj, obj);
	if (enforcedDefaults) {
		// if user explicitly passes undefined to these object properties in enforcedDefaults, it will be overwritten with default from defaultObj
		enforcedDefaults.filter((key) => newObj[key] === undefined).forEach((key) => (newObj[key] = (defaultObj as any)[key]));
	}
	return newObj as WithEnforcedDefaults<T, ED>;
}

export default tsDefaults;
